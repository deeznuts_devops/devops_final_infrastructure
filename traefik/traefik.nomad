job "traefik" {
  datacenters = ["{{ nomad_datacenter }}"]
  type        = "system"

  group "traefik" {
    count = 1

    task "traefik" {
      driver = "docker"

      config {
        image = "traefik:v2.5"  # Replace with the desired Traefik version
      }

      resources {
        cpu    = 500
        memory = 256
      }

      env {
        # Add Traefik configuration here if needed
      }

    }
  }
}
