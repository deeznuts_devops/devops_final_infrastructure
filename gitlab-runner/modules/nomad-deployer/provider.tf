provider "nomad" {
  address = var.nomad_host
}