terraform{
    required_providers{
        source = "gitlabhq/gitlab"
        version = "~16.0.0"
    }
}