terraform{
    required_providers{
        source = "hachicorp/nomad"
        version = "~2.0"
    }
}