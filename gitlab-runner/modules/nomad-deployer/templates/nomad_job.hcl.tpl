job "{{ .job_name }}" {
  datacenters = ["dc1"]

  group "example" {
    count = 1

    task "app" {
      driver = "docker"

      config {
        image = "{{ .image }}"
      }
    }
  }
}
